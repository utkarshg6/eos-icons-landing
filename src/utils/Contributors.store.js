const contributors = [
  {
    title: 'Cynthia Sanchez on twitter',
    href: 'https://twitter.com/cyntss',
    image: 'image1.png',
    name: 'Cynthia Sanchez'
  },
  {
    title: 'Zvezdana Marjanovic on Linked-In',
    href: 'https://www.linkedin.com/in/zvezdanam',
    image: 'image8.png',
    name: 'Zvezdana Marjanovic'
  },
  {
    title: 'Manuele Carlini on twitter',
    href: 'https://twitter.com/manuelecarlini',
    image: 'image3.png',
    name: 'Manuele Carlini'
  },
  {
    title: 'Richa Bhist on Linked-In',
    href: 'https://www.linkedin.com/in/richabisht/',
    image: 'image6.png',
    name: 'Richa Bhist'
  },
  {
    title: 'Jesus Herman on twitter',
    href: 'https://twitter.com/hesusjerman',
    image: 'image4.png',
    name: 'Jesus Herman'
  },
  {
    title: 'Sorin Curescu on twitter',
    href: 'https://twitter.com/en3sis',
    image: 'image2.png',
    name: 'Sorin Curescu'
  },
  {
    title: 'Abhinandan Sharma on Linked-In',
    href: 'https://www.linkedin.com/in/abhinandan-sharma-672299150/',
    image: 'image5.png',
    name: 'Abhinandan Sharma'
  },
  {
    title: 'Kartikay Bhutani on twitter',
    href: 'https://twitter.com/kbhutani0001',
    image: 'image7.png',
    name: 'Kartikay Bhutani'
  },
  {
    title: 'Carla Moratillo on Dribbble',
    href: 'https://dribbble.com/Carla_Isela',
    image: 'carla.jpg',
    name: 'Carla Moratillo'
  },
  {
    title: 'Saurabh Sharma on Twitter',
    href: 'https://twitter.com/SAURABH20944279?s=09',
    image: 'saurabh.jpg',
    name: 'Saurabh Sharma'
  },
  {
    title: 'Kateryna Marchak on Twitter',
    href: 'https://twitter.com/ZenZippy',
    image: 'kathie.png',
    name: 'Kateryna Marchak'
  },
  {
    title: 'Rishabh kohale on LinkedIn',
    href: 'https://www.linkedin.com/in/r-kohale9/',
    image: 'rishab.jpg',
    name: 'Rishabh Kohale'
  }
]

export default contributors
